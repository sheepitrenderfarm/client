package com.sheepit.client.zip;

import lombok.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

public class ChunkInputStream extends InputStream {
	
	@NonNull private final Deque<File> chunkPaths;
	@NonNull private InputStream currentStream;
	
	/**
	 * Given a list of chunk paths, provides an InputStream that reads the contents of these files in the order they were provided in.
	 * @param chunkPaths Non-empty, ordered list of paths
	 * @throws IOException If the first chunk could not be found. Errors with other chunk will be thrown during calls to read()
	 */
	public ChunkInputStream(List<File> chunkPaths) throws IOException {
		this.chunkPaths = new ArrayDeque<>(chunkPaths);
		
		// Setup the first chunk for reading
		currentStream = new FileInputStream(this.chunkPaths.removeFirst());
	}
	
	@Override public int read() throws IOException {
		while (true) {
			int result = currentStream.read();
			// Finished reading from this chunk, continue with the next if possible
			if (result > 0 || tryOpenNextChunk() == false) {
				return result;
			}
		}
	}
	
	@Override public int read(byte[] b, int off, int len) throws IOException {
		while (true) {
			var read = currentStream.read(b, off, len);
			// Finished reading from this chunk, continue with the next if possible
			if (read > 0 || tryOpenNextChunk() == false) {
				return read;
			}
		}
	}
	
	@Override public void close() throws IOException {
		currentStream.close();
	}
	
	private boolean tryOpenNextChunk() throws IOException {
		if (chunkPaths.isEmpty()) {
			return false;
		}
		currentStream.close();
		currentStream = new FileInputStream(chunkPaths.removeFirst());
		return true;
	}
}
