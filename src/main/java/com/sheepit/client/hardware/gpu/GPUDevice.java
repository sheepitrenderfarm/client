/*
 * Copyright (C) 2013-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.sheepit.client.hardware.gpu;

import lombok.Data;

@Data
public class GPUDevice {
	private String type;
	private String model;
	private long memory; // in B
	
	private String id;
	
	private String oldId; // for backward compatibility
	
	private String driverVersion;
	
	public GPUDevice(String type, String model, long ram, String id) {
		this.type = type;
		this.model = model;
		this.memory = ram;
		this.id = id;
	}
	
	public GPUDevice(String type, String model, long ram, String id, String oldId) {
		this(type, model, ram, id);
		this.oldId = oldId;
	}
	
	@Override public String toString() {
		return "GPUDevice [type=" + type + ", model='" + model + "', memory=" + memory + ", id=" + id + ", driverVersion=" + driverVersion + "]";
	}
	
	public static int compareVersions(String version1, String version2) {
		int comparisonResult = 0;
		
		String[] version1Splits = version1.split("\\.");
		String[] version2Splits = version2.split("\\.");
		int maxLengthOfVersionSplits = Math.max(version1Splits.length, version2Splits.length);
		
		for (int i = 0; i < maxLengthOfVersionSplits; i++){
			Integer v1 = i < version1Splits.length ? Integer.parseInt(version1Splits[i]) : 0;
			Integer v2 = i < version2Splits.length ? Integer.parseInt(version2Splits[i]) : 0;
			int compare = v1.compareTo(v2);
			if (compare != 0) {
				comparisonResult = compare;
				break;
			}
		}
		return comparisonResult;
	}
}
