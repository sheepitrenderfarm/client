package com.sheepit.client.hardware.hwid;

import com.sheepit.client.logger.Log;
import com.sheepit.client.hardware.hwid.impl.BaseHWInfoImpl;

import java.io.File;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

public class HWIdentifier {
	private final BasicHWInfoStrategy strategy;
	private Log log;
	
	public HWIdentifier(Log log) {
		strategy = new BaseHWInfoImpl();
		this.log = log;
	}
	
	public String getHardwareHash() {
		try {
			String hwid = String.format("%1.16s-%1.16s-%1.16s", hashHWIdPart(strategy.getMAC()), hashHWIdPart(strategy.getHarddriveID()),
				hashHWIdPart(strategy.getProcessorName()));
			
			//Fallback: computing a hash out of homepath+jarFileLocation
			if ("null-null-null".equals(hwid)) {
				log.debug("Hardware::loaded Hardware definitions fallback");
				
				String homeDir = System.getProperty("user.home"); //get home path
				URL clientURL = getClass().getProtectionDomain().getCodeSource().getLocation();
				String clientPath = new File(clientURL.toString()).getParent(); //get jar file location
				
				hwid = hashHWIdPart(Optional.of(homeDir + clientPath));
			}
			
			return hwid;
		}
		catch (Exception e) {
			log.error("HWIdentifier::getHardwareHash could not retrieve hash: " + e);
			return "unknown";
		}
	}
	
	private String hashHWIdPart(Optional<String> part) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("md5");
		
		return part //
			.map(s -> digest.digest(s.getBytes(StandardCharsets.UTF_8))) //
			.map(b -> new BigInteger(1, b).toString(16)) //
			.orElse("null");
	}
}
